function storeTrade(initState) {

  return {
    name: 'trade',
    state: initState,
    model: StateTrade,
  };

  function StateTrade(data = {}, prev = {}) {
    this.trades = data.trades;
    
    // Actions
    this['STORE_TRADE'] = ({ trade, coins = [] }) => ({
      trade: new Trade(trade, coins)
    })
    this['STORE_TRADES'] = ({ trades, coins = [] }) => ({ 
      trades: trades.map( trade => new Trade(trade, coins))
    })
  }

  function Trade({ id, from, to, type, fromAmount, toAmount, createdAt, updatedAt, status, index, price, merchant, transaction }, coins) {
    
    const fromCoin = coins && coins.find( coin => coin.symbol.toLowerCase() === (from && from.toLowerCase()));
    const toCoin = coins && coins.find( coin => coin.symbol.toLowerCase() === (to && to.toLowerCase()));
    
    const fromFiat = from === 'USD';
    const toFiat = to === 'USD';
    
    this.id = id;
    this.merchant = merchant;
    this.transaction = transaction && transaction[0] || {};
    this.index = index;
    this.price = price;
    this.from = from && from.toUpperCase();
    this.to = to && to.toUpperCase();
    this.fromAmount = fromFiat ? Math.round(fromAmount * 100) / 100 : Math.round(fromAmount * 1000000) / 1000000;
    this.toAmount = toFiat ? Math.round(toAmount * 100) / 100 : Math.round(toAmount * 1000000) / 1000000;
    this.type = type;
    this.status = status;
    this.sell = type === 'sell';
    this.fromTypeIconUrl = fromCoin && fromCoin.img || './img/icon-usd.svg';
    this.toTypeIconUrl = toCoin && toCoin.img || './img/icon-usd.svg';
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;
  }
}
