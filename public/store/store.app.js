function storeApp(initState) {

  return {
    name: 'app',
    state: initState,
    model: StateApp,
  };

  function StateApp(data = {}, prev = {}) {

    this.variables = data.variables;
    this.coins = data.coins;
    this.news = data.news;
    this.authors = data.authors;
    this.coin = data.coin;
    this.totalcap = data.totalcap;
    this.coincap = data.coincap;
    this.USDImgURL = 'https://cdn.coinranking.com/OjL-P17sq/usd.svg';
    this.backURL = null;
    this.graphcms = {
      endpoint : 'https://api-apeast.graphcms.com/v1/',
      GQ       : 'cjttdugp21u1t01dva4dmkoq5/master',
      token    : 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJ0b2tlbklkIjoiYzllNWFmNmItOWI1YS00YmI3LTg2NGEtZGM3YjgxZjg1MGJiIn0.ISnn54uojqjUhJILV1UUAfAQ4fNsaQXG_psX4ivRlE8'
    };
    // Actions
    this['STORE_USER'] = ({ user }) => ({ user });
    this['STORE_COINS'] = ({ coins }) => ({
      coins: coins.map(coin => new Coin(coin, this.variables.spread))
    });

    this['STORE_NEWS'] = ({ news, cryptoasia }) => {
      let authors = news.reduce((arr, item) => {
        !~arr.indexOf(item.source) && arr.push(item.source);
        return arr;
      }, ['cryptoasia']);
      
      news = news.concat(cryptoasia);
      news = Array.from(news).map(article => new News(article));

      return {
        authors,
        news: news.sort((a, b) => new Date(b.date) - new Date(a.date))
      }
    }

    this['STORE_COIN'] = symbol => ({
      coin: this.coins.find(coin => coin.symbol === symbol)
    });

    this['STORE_TOTAL_CAP'] = ({ market_cap_by_available_supply }) => ({
      totalcap: market_cap_by_available_supply
        .map(frag => new Fragment(frag))
        .filter(frag => frag[0] >= new Date((new Date().setFullYear(new Date().getFullYear() - 1))).getTime())
    });

    this['STORE_COIN_CAP'] = ({ data: { history } }) => ({
      coincap: history.map(frag => new Fragment(frag))
    });

  }

  function Coin(props = {}, spread = 0.05) {
    const { price, iconUrl, symbol, name, marketCap, volume, change, id, color, description } = props || {};
    spread = parseFloat(spread);
    this.symbol = symbol;
    this.buy = parseFloat((parseFloat(price) * (1 - spread)).toFixed(2));
    this.sell = parseFloat((parseFloat(price) * (1 + spread)).toFixed(2));
    this.price = parseFloat(price).toFixed(2);
    this.currency = 'USD';
    this.img = iconUrl;
    this.name = name;
    this.cap = marketCap;
    this.vol = volume;
    this.change = change;
    this.coinRankId = id;
    this.coinRankColor = color;
    this.desc = description;
    this.margin = spread;
  }

  function News({ title, link, date, desc, img, url, imageurl, body, author, source, published_on, publishdate, id , content, isShowed}) {
    this.title = title;
    this.link = link || url;
    this.date = publishdate || date || published_on * 1000;
    this.desc = desc || body;
    this.img = img || imageurl;
    this.url = url;
    this.author = author || source || 'cryptoasia';
    this.id = id;
    this.content = content;
    this.desc = this.desc && this.desc.length > 250 ? this.desc.substring(0, 250) : this.desc;
    this.desc = this.desc && this.desc.includes('...') ? this.desc : this.desc + '...';
    this.isShowed = isShowed || false;
    
    // this.author === 'cryptoasia' && console.log(this, date);
  }

  function Fragment(frag) {
    if (Array.isArray(frag))
      return [frag[0], frag[1]];

    return [frag.timestamp, parseFloat(parseFloat(frag.price).toFixed(2))];
  }

  // function Coin(data = {}){
  //   this.name     = data.name || 'none';
  //   this.website  = data.extras && data.extras.website;
  //   this.updated  = data.extras && data.extras.timestamp;
  //   this.symbol   = data.symbol;
  //   this.rank     = data.rank;
  //   this.link     = data.link;
  //   this.logo     = data.logo || data.id 
  //     && `https://www.livecoinwatch.com/images/icons32/${data.symbol.toLowerCase()}.png`
  //     //`https://files.coinmarketcap.com/static/img/coins/32x32/${data.id}.png`;
  //   this.supply   = data.supply;
  //   this.listed   = data.listed;
  //   this.markets  = data.extras   && data.extras.markets && getMarkets(data.extras.markets, this.symbol);
  //   this.price    = data.markets  && getPrice(this.markets);
  //   this.stats    = data.stats    && new Stats(data.stats);
  //   this.stage    = getStage(this.listed);
  //   this.tags     = getTags(this);
  //   return this;


  //   function getMarkets(markets, symbol){
  //     return Object.keys(markets).map(id => new Market(markets[id], symbol)).sort((a, b) => b.pc - a.pc);
  //   }

  //   function getTags({ name, symbol, markets, stage }){
  //     let tags = [name.toLowerCase(), symbol.toLowerCase(), stage.toLowerCase()];
  //     /** TODO!!!
  //     *  1. remove special letters  
  //     */
  //     return markets && tags.concat(markets.slice().map(market => market.name.toLowerCase())) || tags;
  //   }

  //   function getPrice(markets){
  //     let market = markets.slice().shift();
  //     return {
  //       usd: market && market.price && market.price.usd || 'NA',
  //       btc: market && market.price && market.price.btc || 'NA'
  //     };
  //   }

  //   function getStage(listed){
  //     let now = new Date().getTime();
  //     let day = 24 * 60 * 60 * 1000;
  //     if(listed + (2 * 30 * day) > now)
  //       return 'fresh';
  //     if(listed + (8 * 30 * day) > now)
  //       return 'new';
  //     if(listed + (24 * 30 * day) > now)
  //       return 'neo';
  //     return 'old';
  //   }
  // }

  // function Stats(data){
  //   this.rank   = parseInt(data.rank);
  //   this.price  = {
  //     usd: Number(data.price_usd).toFixed(2),
  //     btc: Number(data.price_btc).toFixed(2),
  //     sat: parseInt(Number(data.price_btc) * 100000000)
  //   };
  //   this.change = {
  //     hour:   Number(data.percent_change_1h),
  //     day:    Number(data.percent_change_24h),
  //     weak:   Number(data.percent_change_7d),
  //   };
  //   this.up     = this.change.day > 0;
  //   this.supply = Number(data.available_supply || data.total_supply || data.max_supply);
  //   this.cap    = Number(data.market_cap_usd);
  //   this.volume = Number(data['24h_volume_usd']);

  //   this.shortVolume  = short(this.volume);
  //   this.shortSupply  = short(this.supply);
  //   this.shortCap     = short(this.cap);

  //   //ripple supply
  //   this.estimated = (this.supply * Number(this.price.usd) / 39000000000).toFixed(3);

  //   this.trash = this.volume < 400000;

  //   return this;

  //   function short(val){
  //     if(val > 1000000000)
  //       return (val / 1000000000).toFixed(1) + 'kkk';
  //     if(val > 1000000)
  //       return (val / 1000000).toFixed(1) + 'kk';
  //     if(val > 1000)
  //       return (val / 1000).toFixed(1) + 'k';

  //     return val;
  //   }
  // }

  // function Market({ name, volume, price, pc, exchanges, id }, symbol){
  //   this.id     = id;
  //   this.name   = name;
  //   this.volume = volume;
  //   this.price  = price;
  //   this.pc     = pc;

  //   this.exchanges = exchanges.map(exchange => ({
  //     symbol: exchange.symbol,
  //     link:   exchange.link,
  //     type:   exchange.symbol.split('/'),
  //   })).filter(exchange => exchange.type[0] === symbol);

  //   return this;

  // }
}
