function userActions(){
  
  const GQL_ENDPOINT = 'https://cryptoasia.co/v1/graphql';
  const API_ENDPOINT = 'https://cryptoasia.co/api' || 'https://poinout-noneede.c9users.io:8081/';
  
  return {
    
    USER_MERCHANT: function(token, userId){
      token = token || this.store('def').get('cookies.token');
      
      return this.utils.gql.post({
        endpoint: GQL_ENDPOINT,
        token,
        query: `{
          Merchant(where: {userId: {_eq: "${userId}"}}) {
            margin
            id
            userId
            destinations
            maxAmount
            minAmount
            isActive
          }
        }`
      }).then( ([ merchant ]) => merchant);
    },
    
    USER_MERCHANT_UPDATE: function(value, data){
      const token = this.store('def').get('cookies.token');
      const user = this.store('app').get('user');
      
      const opts = {
        endpoint: GQL_ENDPOINT,
        token,
        query: `mutation {
          update_Merchant(where: {userId: {_eq: "${user.id}"}}, _set: {isActive: ${value}}) {
            returning {
              isActive
            }
          }
        }`
      };
      
      return this.utils.gql.post(opts)
        .then( ({ returning: [merchant]}) => {
          this.store('app').set('STORE_USER', { user: {...user, merchant: { ...user.merchant, ...merchant }}});
          return ({ loading: false })
        })
    },

    USER_SET: function(token){
      
      token = token || this.store('def').get('cookies.token');
      return this.utils.gql.post({
        endpoint: GQL_ENDPOINT,
        token,
        query: `query { 
          Credentials(where: { token: {_eq: "${token}"} }) { 
            user { id role } 
          }
        }`
      }).then( ([ credentials ]) => {
        const { user } = credentials;
        // const { user } = credentials || {};
        
        if(user.role !== 'merchant'){
          const state = this.store('app').set('STORE_USER', { user });
          return state;
        }
        return this.act('USER_MERCHANT', token, user.id).then(merchant => {
          const state = this.store('app').set('STORE_USER', { user: { ...user, merchant } });
          return state;
        })
      })
    },
    
    USER_LOGOUT: function(){
      this.act('DEF_REMOVE_COOKIE', 'token');
      this.store('app').set('STORE_USER', { user: null });
      this.utils.gql.close({ url: GQL_ENDPOINT });
      this.store('trade').set('trades', null);
      console.log('CLOSE SOCKET');
      return this.act('DEF_REDIRECT', '/');
    },
    
    USER_LOGIN: function(body){
      
      return window.fetch(API_ENDPOINT + '/auth', {
        method: 'POST',
        body: JSON.stringify(body), 
        headers: { 'content-type': 'application/json' } 
      })
      .then( res => res.json() )
      .then( (data = {}) => ({ ...( typeof data === 'object' && data || {}), error: data.message, loading: false }) )
      // .then( data => (console.log(data) || data))
    },
    
    USER_VERIFY: function(body){
      
      body = { ...body, pin: body.code };
      
      return window.fetch(API_ENDPOINT + '/auth', {
        method: 'POST',
        body: JSON.stringify(body), 
        headers: { 'content-type': 'application/json' } 
      })
      .then( res => res.json() )
      .then( ({ token }) => {
        
        if(!token)
          return ({ error: 'Incorrect Credentials', loading: false })
          
        this.act('DEF_SET_COOKIE', { token });
        // this.act('USER_SET', token).then( () =>
        const trade = this.store('trade').get('trade');
        
        trade && trade.fromAmount
          ? this.act('TRADE_INSERT', null, token)
          : this.act('DEF_REDIRECT', '/trades');
          
        return {};
        
      })
    },
    
    USER_NEW_PIN: function(body){
      return this.utils.post({ url: API_ENDPOINT + '/encryptPin', body })
        .then(({ message, data }) => {
          if(message)
            return ({ loading: null, error: message });
          
          return ({ loading: null });
        })
    }
  }
}
