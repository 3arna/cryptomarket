function tradesActions() {
  const GQ = 'cjfanw29d61jm01856zsz9an8';
  const API_ENDPOINT = 'https://cryptoasia.co/api';
  const GQL_ENDPOINT = 'https://cryptoasia.co/v1/graphql';

  return {
    TRADE_SUBSCRIBE: function({ url }){
      
      const coins = this.store('app').get('coins');
      
      const subscription = {
        id: 'trades',
        action: (trades) => {
          this.store('trade').set('STORE_TRADES', { trades, coins });
          this.act('DEF_RESTATE');
        },
        query: `subscription {
          Trade(order_by: { createdAt: desc}) {
            createdAt
            from
            fromAmount
            id
            status
            to
            toAmount
            type
            index
            price
            merchant { id margin }
            transaction {
              id
              buyersDestination
              buyerAgrees
              buyersConfirmation
              buyerConfirms
              sellersDestination 
              sellersConfirmation
              sellerAgrees
              sellerConfirms
            }
          }
        }`
      }
      
      this.utils.gql.subscribe({ url, subscription })
    },
    
    TRADE_CALCULATE: function(value, data){
      
      const { name } = data || {};
      const { from, to, fromAmount, toAmount } = this.store('trade').get('trade');
      const isToUSD = to === 'USD';
      
      const body = typeof value !== 'string'
        ? { ...( isToUSD ? { fromAmount: toAmount} : { toAmount: fromAmount }), from: to, to: from }
        : { [name]: value, from, to }
      
      return this.utils.post({ url: API_ENDPOINT + '/trade?calculate', body })
        .then(({ message, data }) => {
          if(message)
            return ({ loading: null, error: message });
            
          this.store('trade').set('STORE_TRADE', { 
            trade: data, 
            coins: this.store('app').get('coins')
          });
          
          return ({ loading: null });
        })
      
    },
    
    TRADE_UPDATE: function(value, data) {
      
      let body = {};
      if(typeof value === 'string'){
        const { name, trade } = data;
        body[name] = value;
        trade && trade.id && (body.id = trade.id);
      } else 
        body = value;
      
      const token = this.store('def').get('cookies.token');
      
      return this.utils.post({ url: API_ENDPOINT + '/trade', body, token })
        .then(res => ({ loading: null, error: res.message }))
        
    },
    
    TRADE_TRANSACTION_UPDATE: function(value, data){
      const body = {};
      const { name, transaction } = data;
      body[name] = value;
      transaction && transaction.id && (body.id = transaction.id);
      const token = this.store('def').get('cookies.token');
      
      return this.utils.post({ url: API_ENDPOINT + '/transaction', body, token })
        .then(res => console.log(res) || ({ loading: null, error: res.message }))
    },
    
    TRADE_SWAP: function(trade){
      
      const body = {
        to: trade.from,
        from: trade.to
      }
      
      trade.from === 'USD' 
        ? body.toAmount = trade.fromAmount
        : body.fromAmount = trade.toAmount;
        
      trade && trade.id && (body.id = trade.id);
      
      return this.act('TRADE_UPDATE', body);
    },
    
    TRADE_INSERT: function(body, token){
      body = body || this.store('trade').get('trade');
      token = token || this.store('def').get('cookies.token');
      
      if(!token)
        return this.act('DEF_REDIRECT', '?splash=login');
        
      return this.utils.post({ url: API_ENDPOINT + '/trade', token, body })
        .then(({ message, data }) => data && data.id 
          ? this.act('DEF_REDIRECT', '/trade?id=' + data.id )
          : ({ error: message, loading: null }) )
    },
    
    TRADE_DELETE: function({ id }){
      const token = this.store('def').get('cookies.token');
      return this.utils.gql.post({
        endpoint: GQL_ENDPOINT,
        token,
        query: `mutation {
          delete_Trade(where: {id: {_eq: "${id}"}}) {
            affected_rows
          }
        }`
      }).then(res => this.act('DEF_REDIRECT', this.store('def').get('back') || '/trades') );
    }
  }
}
