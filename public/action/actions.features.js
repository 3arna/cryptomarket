function featuresActions(){
  return {
    FEATURES_FETCH: function (variables) {
      const app = this.store('app').get();
      
      //Store graph cms endpoint
      const endpoint  = app.graphcms.endpoint;
      const GQ        = app.graphcms.GQ;
      const token     = app.graphcms.token;
      
      return this.utils.gql.post({
        GQ,
        token,
        endpoint,
        variables,
        query:`
          query{
            features(where: { status: PUBLISHED }){
              id
              isDone
              description
              title
              updatedAt
            }
          }
        `
      });
    }
  }
}