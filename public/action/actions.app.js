function appActions() {

  return {

    APP_INIT: function(data) {
      return this.act('APP_CONTENT', data);
    },

    APP_CONTENT: function({ coins, news, variables, totalcap, coincap, cryptoasia }) {
      
      cryptoasia = cryptoasia && cryptoasia.map( (article = {}, i) => article && ({
        ...article,
        desc: article.description,
        date: article.publishdate,
        img: article.imageurl && article.imageurl.url,
        description: null
      }) || {}) || [];
      
      this.store('app').set({ variables });
      this.store('app').set('STORE_NEWS', { news: news.Data, cryptoasia });
      this.store('app').set('STORE_COINS', { coins: coins.data.coins });
      this.store('app').set('STORE_COIN', 'BTC');
      this.store('app').set('STORE_TOTAL_CAP', totalcap);
      this.store('app').set('STORE_COIN_CAP', coincap);
      
      this.store('trade').set('STORE_TRADE', { 
        trade: { to: 'BTC', from: 'USD' }, 
        coins: this.store('app').get('coins')
      });
    
      return Promise.resolve(this.store('app').get());
    },

    APP_AUTH: function(data = {}) {
      const { cookies, page } = this.store('def').get();
      const user = this.store('app').get('user');

      if(user || !cookies.token)
        return Promise.resolve({ user });
      
      // We should auth on every initial load
      
      return this.act('USER_SET', cookies.token)
        .then(data => this.act('APP_WS_INIT', cookies.token) && data);

    },

    APP_ROUTE: function() {
        
      const defState = this.store('def').get();
      const { user, variables, coins, news, authors, marketcap, coin, totalcap, coincap } = this.store('app').get();
      
      let { page, params, meta } = defState;
      const article = news && page.name === 'article' && news.find(article => article.link === params.link);
      
      if(article)
        meta = {
          ...meta,
          title: 'CrytpoAsia - ' + article.title,
          description: 'CrytpoAsia - ' + article.desc,
          image: article.img,
          url: 'https://cryptoasia.co/' + article.link
        }
      
      return Promise.resolve(Object.assign({}, defState, {
        coins,
        user,
        news,
        authors,
        marketcap,
        coin,
        totalcap,
        coincap,
        meta
      }));
    },

    // APP_ROUTE_ORDER: function({ user: { orders } }) {
    //   const { id } = this.store('def').get('params');
    //   return Promise.resolve({
    //     title: 'order',
    //     order: id && orders && orders.find(order => order.id === id) || {},
    //   });
    // },

    // APP_ROUTE_ORDERS: function({ user: { orders } }) {
    //   return Promise.resolve({
    //     title: 'orders',
    //     orders
    //   });
    // },

    // APP_ROUTE_TRADE: function({ user }) {
    //   return Promise.resolve({
    //     trade: this.store('trade').get()
    //   });
    // },

    APP_GET_COIN_HISTORY: function(coinRankId) {
      return fetch('https://api.coinranking.com/v1/public/coin/' + coinRankId + '/history/1y').then(res => res.json())
    },

    APP_SET_COIN: function({ symbol, redirect }) {
      const { coin: { coinRankId } } = this.store('app').set('STORE_COIN', symbol);
      return this.act('APP_GET_COIN_HISTORY', coinRankId).then(history => {
        this.store('app').set('STORE_COIN_CAP', history);
        redirect
          ? this.act('DEF_REDIRECT', redirect) 
          : this.act('DEF_RESTATE');
      });
    },
    //update trade table
    // APP_WS_DATA: function(data) {
    //   const { node } = data;
    //   this.act('TRADE_FETCH', { userId: node.user.id , mode: 0}).then(t => {
    //     const { trades } = this.store('trade').get();
    //     if(trades){
    //       Object.assign(trades, t); 
    //       this.act('DEF_RESTATE');
    //     }
    //   });

    //   // this.act('USER_SET').then(({ user }) => this.act('DEF_RESTATE'));
    // },

    APP_WS_INIT: function(token) {
      console.log('OPEN SOCKET');
      if (this.SERVER)
        return;
      
      const url = 'wss://cryptoasia.co/v1/graphql';
      
      return this.utils.gql.open({ url, token })
        .then( socket => {
          this.act('TRADE_SUBSCRIBE', { url });
        })
    }
  }
}
