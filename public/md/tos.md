# CryptoAsia Exchange Terms of Use

The [CryptoAsia Digital Currency Exchange](https://marketplace.cryptoasia.co) (the “**Exchange**”) Terms of Use (the “**Terms**”) govern 
customer (the “**Customer**”) usage of digital currency exchange services (the “**Services**”) provided by
CryptoAsia at [https://marketplace.cryptoasia.co](https://marketplace.cryptoasia.co). By using the Services, you agree to the Terms and
confirm that you accept them.

## The Services

The Services constitute any action by CryptoAsia to help facilitate the purchase and sale of digital
currencies within Cambodia by the Customer.  

The Services do not constitute a digital currency wallet, storage mechanism, or custodial services for
digital currency at any time on behalf of the Customer.

## Rights and Obligations

CryptoAsia provides the Services made possible through its proprietary exchange platform. The
Customer may make use of the Exchange for the purposes of purchasing digital currencies such as
Bitcoin and Ethereum inside of Cambodia. The Customer guarantees the integrity and accuracy of all
data submitted to CryptoAsia via the Services or by any other means.

## Registration

In order to use the Services, the Customer must open an Exchange account by registering with a
Cambodian phone number.

## Transaction Volume Limits

CryptoAsia imposes daily and annual transaction processing limits on all Customers of the Exchange.
Limits are determined at the sole discretion of CryptoAsia and subject to change without notice.

## Representation and Warranties

Use of the Services is subject to the laws and regulations of the Kingdom of Cambodia. The Customer
agrees and acknowledges that use of the Services will comply with such laws and regulations without
limitation.  

*Customer use of the Services is also subject to the following important restrictions. You, the Customer,
warrant to CryptoAsia that:*

* You are at least 18 years old and have the right, power and contractual capacity to agree to these Terms
* Your use of the Services does not constitute action on behalf of any 3 rd party
* You are the sole owner of any digital currency addresses that you submit

## Right to Reject

CryptoAsia reserve the right to decline to process a sale if we believe that it violates these Terms or
would expose anyone to harm. If CryptoAsia reasonably suspects that the Customer account has been
used for an illegal purpose then you authorize us to share information about you or your account with
law enforcement.

## Third Parties

CryptoAsia expressly disclaims any liability for third-party services. By using the Services offered by
CryptoAsia the Customer acknowledges he is acting on his own behalf and at his own discretion.

## Proprietary Software

The Exchange offered by CryptoAsia is proprietary and we cannot guarantee there will be no bugs in the
software. Customer acknowledges that use of the Exchange is at his own risk.

## Fees & Exchange Rates

Digital currency fees and exchange rates are adjusted automatically by the Exchange. Real-time rates are
made plainly visible to the Customer on the Exchange platform at all times.

## Inaccurate Payments
Partially paid digital currency orders will not be filled until the full amount has been remitted by the
Customer to CryptoAsia. Overpayments will be refunded to the Customer in cash. CryptoAsia reserves
the right to cancel any order whose payment is inaccurate and refund it to the Customer.

## Payment Networks & Systems

CryptoAsia will not be liable for errors arising from external payment systems or protocols including
money transfer services and digital currency protocols. Errors include but are not limited to stuck
transactions due to insufficient fees, technical failures, account closures, or any technical failure on
account of digital payment networks.

## Refunds
All sales are final and cannot be reversed.

## Indemnity
The customer agrees to indemnify CryptoAsia and any individuals or entities derivative therefrom
against any loss, damage or liability resulting from a third-party claim, demand, action or proceeding
that arises out of these Terms or the Exchange, including but not limited to your use, non-use or misuse
of the Exchange or the Services.

## Account Termination
We retain the right to close your account, at our discretion, upon notice to you. We may also suspend
your access to the Services if we suspect that you have failed to comply with these Terms, pose an
unacceptable fraud risk to us, or if you provide any false, incomplete, inaccurate or misleading
information. We will not be liable to you for any losses that you incur in connection with our closure or
suspension of your account.