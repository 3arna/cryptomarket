console.log(process.env.NODE_ENV);

const CONFIG = {
  url: 'https://poinout-noneede.c9users.io/',
  gq:  'cjfanw29d61jm01856zsz9an8' || 'cjjim9w1d0l0f0142eoget9k7',
  reload: 30,
  fetcher: [
    {
      name: 'coins',
      url:  'https://api.coinranking.com/v1/public/coins?symbols=BTC,ETH,BCH,EOS,XLM,LTC,ETC,ADA,OMG&sort=coinranking&timePeriod=24h',
    },
    {
      name: 'news',
      url:  'https://min-api.cryptocompare.com/data/v2/news/?lang=EN',
    },
    { 
      name: 'coincap',
      url: 'https://api.coinranking.com/v1/public/coin/1335/history/1y'
    },
    { 
      name: 'totalcap',
      url: 'https://graphs2.coinmarketcap.com/global/marketcap-total/'
    },
    {
      name: 'cryptoasia',
      endpoint: 'https://api-apeast.graphcms.com/v1/cjttdugp21u1t01dva4dmkoq5/master',
      token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXJzaW9uIjoxLCJ0b2tlbklkIjoiYzllNWFmNmItOWI1YS00YmI3LTg2NGEtZGM3YjgxZjg1MGJiIn0.ISnn54uojqjUhJILV1UUAfAQ4fNsaQXG_psX4ivRlE8',
      query: `
        query{
          articles(where: { status: PUBLISHED }){
            id link title publishdate author description content imageurl { url }
          }
        }
      `
    }
  ]
}

process.env.NODE_ENV === 'development'
  ? require('../../@packs/riothing').server(CONFIG)
  : require('riothing').server(CONFIG)
  
